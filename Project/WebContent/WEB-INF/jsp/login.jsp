<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/login.css">
</head>
<body>


	<form class="form-signin" action="LoginResultServlet" method="POST">
		<div class="body"></div>
		<div class="grad"></div>
		<div class="header">
			<div>
				Your<span>No.1</span>
			</div>
		</div>
		<br>
		<div class="login">
			<c:if test="${loginErrorMessage != null}">
				<p class="red-text center-align">${loginErrorMessage}</p>
				<br>
			</c:if>
			<input type="id" placeholder="LoginId" name="login_id"
				value="${inputLoginId}" required><br>
				 <input type="password"
				placeholder="password" name="password" required><br>
				<br>
			<button class="btn" type="submit" name="action">Login</button>
			<br>
			<div class="col-sm-4" align="center">
				<a href="Toroku1Servlet">新規登録</a>
			</div>
		</div>
	</form>
</body>
</html>