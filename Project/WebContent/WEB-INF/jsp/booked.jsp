<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/booked.css">

<title>予約済み</title>
</head>
<body>

	<form class="">
		<div class="col-sm-11" align="left">
			<a href="IndexServlet" class="btn btn-svg"> <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="50"></rect>
  </svg> <span>Your No.1</span>
			</a>
		</div>

		<div class="col-sm-12" align="center">
			<h1>予約済み</h1>
		</div>
		<br> <br>

		<c:forEach var="book" items="${book}">
			<a
				href="Restaurantinfo?restaurant_id=${book.restaurantId}&page_num=${pageNum}">
				<div class="container">
					<div class="center">
						<div class="card bg-dark text-white">
							<img src="img/${book.fileName1}" class="card-img" alt="...">
							<div class="card-img-overlay">

								<h3 class="card-title">${book.name}</h3>
								<h5>
									<p class="card-text">${book.word}</p>
								</h5>
								<br>
								<h5>
									<i><p class="card-text">予約日 : ${book.date}</p></i>
								</h5>
								<h5>
									<i><p class="card-text">予約時間 : ${book.time}</p></i>
								</h5>
								<h5>
									<i><p class="card-text">人数：${book.people}</p></i>
								</h5>
								<h5>
									<i><p class="card-text">要望: : ${book.note}</p></i>
							</div>
						</div>
					</div>
				</div>
			</a>
		</c:forEach>

	</form>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>
</body>
</html>

