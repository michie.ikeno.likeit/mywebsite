<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>

<title>title</title>
<link rel="stylesheet" href="css/searchresult.css">
</head>

<body>
	<div class="col-sm-11" align="left">
		<a href="IndexServlet" class="btn btn-svg"> <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="50"></rect>
  </svg> <span>Your No.1</span>
		</a>
	</div>

	<form action="Searchresult" method="POST">

		<div class="col-sm-11" align="center">


			<i class="material-icons prefix">こだわり：</i> <input type="text"
				name="search_restaurant" size="30" placeholder="    　 エリア・シチュエーション">


			<br> <br> <i class="material-icons prefix">予算：</i> <select
				name="search_price">
				<option value="">指定なし</option>
				<option value="¥1,000~">¥1,000~</option>
				<option value="¥2,000~">¥2,000~</option>
				<option value="¥3,000~">¥3,000~</option>
				<option value="¥4,000~">¥4,000~</option>
				<option value="¥5,000~">¥5,000~</option>
				<option value="¥6,000~">¥6,000~</option>
				<option value="¥7,000~">¥7,000~</option>
				<option value="¥8,000~">¥8,000~</option>
				<option value="¥9,000~">¥9,000~</option>
				<option value="¥10,000~">¥10,000~</option>
				<option value="¥15,000~">¥15,000~</option>
				<option value="¥20,000~">¥20,000~</option>

			</select> <br> <br> <input type=submit value="検索">

		</div>
	</form>

	<br>
	<br>

	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">- - - 検索結果 - - -</h5>
			<p>検索結果${restaurantCount}件</p>
		</div>

		<c:forEach var="restaurant" items="${RestaurantList}">

			<a
				href="Restaurantinfo?restaurant_id=${restaurant.restaurantId}&page_num=${pageNum}">

				<div class="card bg-dark text-white">
					<img src="img/${restaurant.fileName1}" class="card-img" alt="...">
					<div class="card-img-overlay">

						<h3>
							<p class="card-title">${restaurant.name}</p>
						</h3>

						<h6>
							<p class="card-text">${restaurant.word}</p>
						</h6>

						<h6>
							<p class="card-text">${restaurant.price}${restaurant.star}</p>
						</h6>
					</div>
				</div>
			</a>

		</c:forEach>
	</div>





	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>
</body>

</html>