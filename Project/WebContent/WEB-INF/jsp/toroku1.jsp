<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">

    <link rel="stylesheet" href="css/toroku1.css">


    <title>登録画面</title>
    </head>
     <div class="col-sm-11" align="left">
    <a href="LoginServlet" class="btn btn-svg">
  <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="50"></rect>
  </svg>
  <span>Your No.1</span>
</a>
    </div>

    <body>
    <form class="form-signin" action="Toroku2Servlet" method="POST">
  <h1>新規登録</h1>

 <c:if test="${validationMessage != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${validationMessage}
		</div>
</c:if>

        <input type="text"  name="login_id" value="${udb.loginId}" placeholder="ログイン時のID" required>
  <input   type="text" name="name" value="${udb.name}" placeholder="名前" required>
         <input  type="text" name="name2" value="${udb.name2}"  placeholder="ふりがな" required>
  <input type="email"  name="mail" value="${udb.mail}" placeholder="Email" required>
        <input type="text"  name="tel" value="${udb.tel}" placeholder="tel"required>
        <input type="date"  name="birthday" value="${udb.birthday}" placeholder="Birthday" required>
  <input type="text" name="password" placeholder="Password" required>
  <input type="text" name="confirmpassword" placeholder="Password(確認)" required>
 <input type="submit" value="確認">
 </form>
 </body>
 </html>