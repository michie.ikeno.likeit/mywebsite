<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
  <link rel="stylesheet"
    	    href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    	    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
    	    crossorigin="anonymous">

    <link rel="stylesheet" href="css/book1.css">
</head>
<body>
<title>予約（時間)</title>
<form class="form-signin" action="Book2Servlet" method="POST">
<c:if test="${validationMessage != null}">
				<P class="red-text">${validationMessage}</P>
			</c:if>
   <div class="body"></div>
    <div class="grad"></div>
    <div class="header">
      <div>Your<span>No.1</span></div>
    </div>
    <br>
    <div class="login">
        <p><label>日付：<br><input type="date" name="date" required></label></p>
            <p><label>時間：<br><input type="time" name="time" required ></label></p>
            <p><label>人数：<br><input type="text" name="people"placeholder="人数" required></label></p>
<br>
         <input type="submit" value="予約者情報の入力">

    </div>
     </form>
     </body>
     </head>
     </html>