<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>

<link rel="stylesheet" href="css/favorite.css">
<title>お気に入り</title>
</head>
<body>

	<form class="">
		<div class="col-sm-11" align="left">
			<a href="IndexServlet" class="btn btn-svg"> <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="50"></rect>
  </svg> <span>Your No.1</span>
			</a>
		</div>

		<div class="col-sm-12" align="center">
			<h1>お気に入り</h1>
		</div>
		<br> <br>
		<c:forEach var="restaurant" items="${restaurant}">

			<a
				href="Restaurantinfo?restaurant_id=${restaurant.restaurantId}&page_num=${pageNum}">
				<div class="container">
					<div class="center">

						<div class="card bg-dark text-white">
							<img src="img/${restaurant.fileName1}" class="card-img" alt="...">
							<div class="card-img-overlay">

								<h4 class="card-title">${restaurant.name}</h4>
								<h6>
									<p class="card-text">${restaurant.word}</p>
									<p class="card-text">${restaurant.price}${restaurant.star}</p>

								</h6>
							</div>
						</div>
					</div>
				</div>
			</a>

		</c:forEach>


	</form>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>
</body>
</html>