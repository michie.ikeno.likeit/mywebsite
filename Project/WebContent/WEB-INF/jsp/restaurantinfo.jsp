<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<title>title</title>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/restaurantinfo.css">


</head>
<body>
	<form class="">
		<div class="col-sm-11" align="left">
			<a href="IndexServlet" class="btn btn-svg"> <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="50"></rect>
  </svg> <span>Your No.1</span>
			</a>
		</div>
		<div class="col-sm-12" align="right">
			<a href="FavoriteServlet?restaurantId=${restaurant.restaurantId}"
				class="button"> <span>お気に入り</span>
			</a> <br> <br> <a
				href="Book1Servlet?restaurantId=${restaurant.restaurantId}"
				class="btn btn-svg"> <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="45"></rect>
  </svg> <span>予約</span>
			</a>
		</div>
		<div class="col-sm-12" align="center">
			<h1>${restaurant.name}</h1>
		</div>
		<br>

		<div class="container">
			<div class="row center"></div>

			<div id="carouselExampleIndicators" class="carousel slide"
				data-ride="carousel">
				<!-- インディケーター -->
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0"
						class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="img/${restaurant.fileName1}"
							alt="First slide">

						<div class="carousel-caption d-none d-md-block">
							<h3>
								<p>${restaurant.word}</p>
							</h3>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="img/${restaurant.fileName2}"
							alt="Second slide">
						<div class="carousel-caption d-none d-md-block">
							<h3>
								<p>${restaurant.price}<br><br>${restaurant.star}</p>
							</h3>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="img/${restaurant.fileName3}"
							alt="Third slide">
						<div class="carousel-caption d-none d-md-block">
							<h3>
								<p>${restaurant.word2}</p>
							</h3>

						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators"
						role="button" data-slide="prev"> <span
						class="carousel-control-prev-icon" aria-hidden="true"></span> <span
						class="sr-only">前へ</span>
					</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
						role="button" data-slide="next"> <span
						class="carousel-control-next-icon" aria-hidden="true"></span> <span
						class="sr-only">次へ</span>
					</a>
				</div>

			</div>

			<br>
			<div class="col-8 mx-auto">
				<div id="list-example" class="list-group">
					<a class="list-group-item list-group-item-action"
						href="#list-item-1">店舗トップ</a> <a
						class="list-group-item list-group-item-action" href="#list-item-2">メニュー</a>
					<a class="list-group-item list-group-item-action"
						href="#list-item-3">写真</a> <a
						class="list-group-item list-group-item-action" href="#list-item-4">情報</a>
				</div>
				<div data-spy="scroll" data-target="#list-example" data-offset="0"
					class="scrollspy-example">
					<div class="col-sm-11" align="center">
					<br>
						<h1 id="list-item-1">TOP</h1>
						<br>
						<p>${restaurant.detail1}</p>
						<br>
						<h1 id="list-item-2">
							<b><i></i>メニュー</b>
							<br>
						</h1>
						<p>${restaurant.detail2}</p>
						<h1 id="list-item-3">
						<br>
							<b><i>写真</i></b>
							<br>
						</h1>
						<div class="center">
							<p>
								<img src="img/${restaurant.fileName4}">
	</p>
	<br>
	<p>
								<img src="img/${restaurant.fileName5}">
							</p>
						</div>
						<br>
						<h1 id="list-item-4">
							<b><i>情報</i></b>
							<br>
						</h1>
						<div class="box8">
							<p>
								<b>基本情報</b>
							</p>
							<table border="1">
								<tr>${restaurant.detail4}
								</tr>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</form>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>

</body>
</html>