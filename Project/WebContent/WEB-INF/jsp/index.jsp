<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/index.css">
<title>title</title>
</head>

<body>

	<div class="col-sm-11" align="left">
		<a href="" class="btn btn-svg"> <svg>
    <rect x="2" y="2" rx="0" fill="none" width=200 height="50"></rect>
  </svg> <span>Your No.1</span>
		</a>
	</div>
	<br>
	<div class="col-sm-2" align="center">
		<a href="LogoutServlet" class="btn-partial-line">
  <i class="fa fa-caret-right"></i>ログアウト</a>
  </div>


<br>
<br>
	<!-- 検索を押したら -->
	<form action="Searchresult" method="post">

		<div class="col-sm-11" align="center">


			<i class="material-icons prefix">こだわり ： </i>
			<input type="text" name="search_restaurant" size="30" placeholder="    　 エリア・シチュエーション">


<br>
<br>
<i class="material-icons prefix">予算 ： </i>
<select name="search_price">
<option value="">指定なし</option>
<option value="¥1,000~">¥1,000~</option>
<option value="¥2,000~">¥2,000~</option>
<option value="¥3,000~">¥3,000~</option>
<option value="¥4,000~">¥4,000~</option>
<option value="¥5,000~">¥5,000~</option>
<option value="¥6,000~">¥6,000~</option>
<option value="¥7,000~">¥7,000~</option>
<option value="¥8,000~">¥8,000~</option>
<option value="¥9,000~">¥9,000~</option>
<option value="¥10,000~">¥10,000~</option>
<option value="¥15,000~">¥15,000~</option>
<option value="¥20,000~">¥20,000~</option>

</select>
<br>
<br>
<input type=submit value="検索">





	</div>
</form>


	<br>
	<br>
	<!-- dogetへ飛ぶ -->
	<div class="col-sm-11" align="center">

		<button type="button" onClick="location.href='./BookedServlet'"
			class="btn btn-secondary" data-toggle="tooltip"
			class="btn btn-secondary" data-toggle="tooltip" data-placement="top"
			title="Tooltip on top">予約済み</button>


		<button type="button" onClick="location.href='./FavoriteServlet'"
			class="btn btn-secondary" data-toggle="tooltip"
			data-placement="right" title="Tooltip on right">お気に入り</button>


		<button type="button" onClick="location.href='./HistoryServlet'"
			class="btn btn-secondary" data-toggle="tooltip"
			data-placement="bottom" title="Tooltip on bottom">閲覧履歴</button>

		<button type="button" onClick="location.href='./Userinfo1Servlet'"
			class="btn btn-secondary" data-toggle="tooltip" data-placement="left"
			title="Tooltip on left">情報更新</button>
	</div>
	<br>
	<br>
	<div class="container">

		<b><i><h5 class=" col s12 light">おすすめ店舗</h5></i></b> <br>
		<br>
		<!--   おすすめ商品   -->
		<div class="container">
			<div class="row center"></div>
			<div class="row center">
				<c:forEach var="restaurant" items="${restaurantList}">

					<section class="card">
						<div class="card-image">
							<a href="Restaurantinfo?restaurant_id=${restaurant.restaurantId}"><img
								src="img/${restaurant.fileName1}" width="350" height="300"></a>
						</div>

						<div class="card-content">
						<p style="text-align:center">
							<span class="card-title">${restaurant.name}</span>

							<p class="card-text">${restaurant.word}</p>
						</div>
						<div class="card-link">
							<a href="Restaurantinfo?restaurant_id=${restaurant.restaurantId}">詳細</a>
						</div>
					</section>

				</c:forEach>

			</div>
		</div>
	</div>

</body>

</html>