package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserBeans;


public class UserDao {

	/**
	 * データの挿入処理を行う。現在時刻は挿入直前に生成
	 *
	 * @param user
	 *            対応したデータを保持しているJavaBeans
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためにスロー
	 */
	//カラムに登録
	public static void insertUser(UserBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO m_user(name,name2,login_id,mail,tel,birthday,password,confirmpassword) VALUES(?,?,?,?,?,?,?,?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getName2());
			st.setString(3, udb.getLoginId());
			st.setString(4, udb.getMail());
			st.setString(5, udb.getTel());
			st.setString(6, udb.getBirthday());
			st.setString(7, udb.getPassword());
			st.setString(8, udb.getConfirmPassword());
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return userbeans (userId,name,mane2,mail,tel)
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static UserBeans getUserId(String loginId,String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT user_id,name,name2,login_Id,mail,tel,birthday,password,confirmpassword FROM m_user WHERE login_id = ? and password = ?");
			st.setString(1, loginId);
			st.setString(2, password);

			//rsにSELECT文の実行結果が入る
			ResultSet rs = st.executeQuery();
			//ログインできなかったら（検索結果がなかったら）if文を通る
			//ログインできた場合はif文を通らない
			//Stringだからnull intだったら数字の0
			if(!rs.next()) {
				return null;
			}

			//結果があったら以下が実行される
			//必要なデータをrsから取得する
					int userId = rs.getInt("user_id");
					String loginId1=rs.getString("login_id");
					String name=rs.getString("name");
					String name2=rs.getString("name2");
					String mail=rs.getString("mail");
					String tel=rs.getString("tel");
					String birthday=rs.getString("birthday");
					String password1=rs.getString("password");

					//変数userbeansにデータをsetする
					UserBeans userbeans = new UserBeans();
					userbeans.setUserId(userId);
					userbeans.setLoginId(loginId);
					userbeans.setName(name);
					userbeans.setName2(name2);
					userbeans.setMail(mail);
					userbeans.setTel(tel);
					userbeans.setBirthday(birthday);
					userbeans.setPassword(password1);




					System.out.println("login succeeded");
			System.out.println("searching userId by loginId has been completed");
			//setしたuserbeansをreturn
			return userbeans;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<UserBeans> findAll() {
		Connection conn = null;
		List<UserBeans> userList = new ArrayList<UserBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			//DB
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM m_user WHERE user_id not in (1) ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				String name2 = rs.getString("name2");
				String loginId = rs.getString("login_id");
				String mail = rs.getString("mail");
				String tel = rs.getString("tel");
				String birthday = rs.getString("birthday");
				String password = rs.getString("password");
				String confirmpassword = rs.getString("confirmpassword");
				UserBeans user = new UserBeans(userId, name, name2,loginId, mail, tel,birthday, password,confirmpassword);

				userList.add(user);//配列の時のみ
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * loginIdの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM m_user WHERE login_id = ? AND user_id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;


	}
	//詳細メソッド
		public UserBeans selectUser(String userId1) {
			Connection conn = null;
			try {
				//DBに接続
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM m_user WHERE user_id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, userId1);

				//実行結果を習得する//実装
				ResultSet rs = pStmt.executeQuery();

				if (!rs.next()) {
					return null;
				}

				//()の中はカラム名 //インスタフィールド
				int userId = rs.getInt("user_id");
				String name = rs.getString("name");
				String name2 = rs.getString("name2");
				String loginId = rs.getString("login_id");
				String mail = rs.getString("mail");
				String tel = rs.getString("tel");
				String birthday = rs.getString("birthday");
				String password = rs.getString("password");


				UserBeans a = new UserBeans(userId,name,name2,loginId,mail,tel, birthday,password) ;
				return a;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}
		/**
		 * ユーザーIDからユーザー情報を取得する
		 *
		 * @param useId
		 *            ユーザーID
		 * @return udbList 引数から受け取った値に対応するデータを格納する
		 * @throws SQLException
		 *             呼び出し元にcatchさせるためスロー
		 */
		public static UserBeans getUserDataBeansByUserId(int userId) throws SQLException {
			UserBeans udb = new UserBeans();
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("SELECT user_id,name,name2,login_id,mail,tel,birthday,passsord FROM m_user WHERE user_id =" + userId);
				ResultSet rs = st.executeQuery();

				while (rs.next()) {
					udb.setUserId(rs.getInt("user_id"));
					udb.setName(rs.getString("name"));
					udb.setName2(rs.getString("name2"));
					udb.setLoginId(rs.getString("login_id"));
					udb.setMail(rs.getString("mail"));
					udb.setTel(rs.getString("tel"));
					udb.setBirthday(rs.getString("birthday"));
					udb.setPassword(rs.getString("password"));

				}

				st.close();

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
			System.out.println("searching UserDataBeans by userId has been completed");
			return udb;

}
		/**
		 * ユーザー情報の更新処理を行う。
		 *
		 * @param user
		 *            対応したデータを保持しているJavaBeans
		 * @throws SQLException
		 *             呼び出し元にcatchさせるためにスロー
		 */
		public static void updateUser(UserBeans userbeans) throws SQLException {
			// 更新された情報をセットされたJavaBeansのリスト

			Connection con = null;
			PreparedStatement st = null;

			try {

				con = DBManager.getConnection();
				st = con.prepareStatement("UPDATE m_user SET name=?,name2=?,login_id=?,mail=?,tel=?,birthday=? WHERE user_id=?");
				st.setString(1, userbeans.getName());
				st.setString(2, userbeans.getName2());
				st.setString(3, userbeans.getLoginId());
				st.setString(4, userbeans.getMail());
				st.setString(5, userbeans.getTel());
				st.setString(6, userbeans.getBirthday());
				st.setInt(7, userbeans.getUserId());


				st.executeUpdate();
				System.out.println("update has been completed");

				st = con.prepareStatement("SELECT name, name2, login_id, mail, tel, birthday FROM m_user WHERE user_id");
				ResultSet rs = st.executeQuery();


				while (rs.next()) {
					UserBeans updatedUdb = new UserBeans();
					updatedUdb.setName(rs.getString("name"));
					updatedUdb.setName2(rs.getString("name2"));
					updatedUdb.setLoginId(rs.getString("login_id"));
					updatedUdb.setMail(rs.getString("mail"));
					updatedUdb.setTel(rs.getString("tel"));
					updatedUdb.setBirthday(rs.getString("birthday"));

				}

				st.close();
				System.out.println("searching updated-UserBeans has been completed");

			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}


}
