package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.RestaurantBeans;

/**
 *
 * @author d-yamaguchi
 *
 */

public class RestaurantDao {
	public static ArrayList<RestaurantBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_restaurant ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<RestaurantBeans> restaurantList = new ArrayList<RestaurantBeans>();
			//ずっと回ってくれる
			while (rs.next()) {
				RestaurantBeans restaurant = new RestaurantBeans();
				restaurant.setRestaurantId(rs.getInt("restaurant_id"));
				restaurant.setName(rs.getString("name"));
				restaurant.setStar(rs.getString("star"));
				restaurant.setPrice(rs.getString("price"));
				restaurant.setWord(rs.getString("word"));
				restaurant.setWord2(rs.getString("word2"));
				restaurant.setDetail(rs.getString("detail"));
				restaurant.setDetail1(rs.getString("detail1"));
				restaurant.setDetail2(rs.getString("detail2"));
				restaurant.setDetail3(rs.getString("detail3"));
				restaurant.setDetail4(rs.getString("detail4"));
				restaurant.setFileName1(rs.getString("file_name1"));
				restaurant.setFileName2(rs.getString("file_name2"));
				restaurant.setFileName3(rs.getString("file_name3"));
				restaurant.setFileName4(rs.getString("file_name4"));
				restaurant.setFileName5(rs.getString("file_name5"));
				restaurant.setFileName6(rs.getString("file_name6"));
				restaurant.setFileName7(rs.getString("file_name7"));

				restaurantList.add(restaurant);
			}
			System.out.println("getAllItem completed");
			return restaurantList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 店舗IDによる店舗検索
	 * @param RestaurantId
	 * @return restaurantBeans
	 * @throws SQLException
	 */
	public static RestaurantBeans getRestaurantByRestaurantID(int restaurantId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			//DBに繋げる
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_restaurant WHERE restaurant_id = ?");
			st.setInt(1, restaurantId);

			ResultSet rs = st.executeQuery();

			RestaurantBeans restaurant = new RestaurantBeans();
			if (rs.next()) {
				restaurant.setRestaurantId(rs.getInt("restaurant_id"));
				restaurant.setName(rs.getString("name"));
				restaurant.setStar(rs.getString("star"));
				restaurant.setPrice(rs.getString("price"));
				restaurant.setDetail(rs.getString("detail"));
				restaurant.setDetail1(rs.getString("detail1"));
				restaurant.setDetail2(rs.getString("detail2"));
				restaurant.setDetail3(rs.getString("detail3"));
				restaurant.setDetail4(rs.getString("detail4"));
				restaurant.setWord(rs.getString("word"));
				restaurant.setWord2(rs.getString("word2"));
				restaurant.setFileName1(rs.getString("file_name1"));
				restaurant.setFileName2(rs.getString("file_name2"));
				restaurant.setFileName3(rs.getString("file_name3"));
				restaurant.setFileName4(rs.getString("file_name4"));
				restaurant.setFileName5(rs.getString("file_name5"));
				restaurant.setFileName6(rs.getString("file_name6"));
				restaurant.setFileName7(rs.getString("file_name7"));
			}

			System.out.println("searching restaurant by restaurantID has been completed");

			return restaurant;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	/**
	 * 商品検索
	 * @param searchRestaurant,searchPrice
	 * @param pageNum
	 * @param pageMaxRestaurantCount
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RestaurantBeans> getRestaurantByPlaceName(String searchRestaurant, String searchPrice)
			throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {

			//DBに接続
			con = DBManager.getConnection();

			//場所と予算の検索
			// 全検索
			if (searchRestaurant.length() == 0 && searchPrice.length() == 0) {
				st = con.prepareStatement("SELECT * FROM m_restaurant ORDER BY restaurant_id ASC");

				//ワードのみが入っている時
			} else if (searchRestaurant.length() != 0 && searchPrice.length() == 0) {
				st = con.prepareStatement("SELECT * FROM m_restaurant WHERE word LIKE ? ORDER BY restaurant_id ASC");
				st.setString(1, "%" + searchRestaurant + "%");
				//プライスのみが入っている時
			} else if (searchRestaurant.length() == 0 && searchPrice.length() != 0) {
				st = con.prepareStatement("SELECT * FROM m_restaurant WHERE price LIKE ? ORDER BY restaurant_id ASC");
				st.setString(1, "%" + searchPrice + "%");
			}else if(searchRestaurant.length() != 0 && searchPrice.length() != 0) {
				st = con.prepareStatement("SELECT * FROM m_restaurant WHERE word LIKE ? AND price LIKE ? ORDER BY restaurant_id ASC");
				st.setString(1, "%" + searchRestaurant + "%");
				st.setString(2, "%" + searchPrice + "%");
			} else {

			}

			// SELECTを実行し、結果表を取得

			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantBeans> RestaurantList = new ArrayList<RestaurantBeans>();

			while (rs.next()) {
				RestaurantBeans restaurant = new RestaurantBeans();
				restaurant.setRestaurantId(rs.getInt("restaurant_id"));
				restaurant.setName(rs.getString("name"));
				restaurant.setDetail(rs.getString("detail"));
				restaurant.setPrice(rs.getString("price"));
				restaurant.setStar(rs.getString("star"));
				restaurant.setWord(rs.getString("word"));
				restaurant.setWord2(rs.getString("word2"));
				restaurant.setFileName1(rs.getString("file_name1"));
				restaurant.setFileName2(rs.getString("file_name2"));
				restaurant.setFileName3(rs.getString("file_name3"));

				RestaurantList.add(restaurant);
			}
			System.out.println("get Restaurants by search has been completed");
			return RestaurantList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品総数を取得
	 *
	 * @param searchRestaurant,searchPrice
	 * @return
	 * @throws SQLException
	 */
	public static double getRestaurantCount(String searchRestaurant, String searchPrice) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			//DBに繋げる
			con = DBManager.getConnection();

			String sql = "SELECT COUNT(*) as cnt FROM m_restaurant";

			if (!searchRestaurant.equals("")) {
				sql += " WHERE word LIKE '%" + searchRestaurant + "%'";
			}

			if (!searchPrice.equals("") && !searchRestaurant.equals("")) {

				sql += " AND price LIKE '%" + searchPrice + "%'";

			} else if (!searchPrice.equals("")) {

				sql += " WHERE price LIKE '%" + searchPrice + "%'";

			}

			PreparedStatement pStmt = con.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			double coung = 0.0;

			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}

			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<RestaurantBeans> getRestaurantBeansListByrestaurantId(String Id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_restaurant.restaurant_id,"
							+ " m_restaurant.name,"
							+ " m_restaurant.word,"
							+ " m_restaurant.file_name1"
							+ " FROM m_book"
							+ " JOIN m_restaurant"
							+ " ON m_book.restaurant_id=m_restaurant.restaurant_id"
							+ " WHERE m_book.login_id=?");
			st.setString(1, Id);

			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantBeans> RestaurantList = new ArrayList<RestaurantBeans>();

			while (rs.next()) {
				RestaurantBeans restaurant = new RestaurantBeans();
				restaurant.setRestaurantId(rs.getInt("restaurant_id"));
				restaurant.setName(rs.getString("name"));
				restaurant.setWord(rs.getString("word"));
				restaurant.setFileName1(rs.getString("file_name1"));

				RestaurantList.add(restaurant);
			}

			System.out.println("searching RestaurantList by RestaurantID has been completed");
			return RestaurantList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//お気に入りへの登録
	public static void insertfavoriteRestaurant(int restaurantId1, int userId1) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"INSERT INTO m_favorite(restaurant_id,user_id) VALUES (?,?)");

			st.setInt(1, restaurantId1);
			st.setInt(2, userId1);

			st.executeUpdate();

			System.out.println("inserting favorite has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//お気に入りのレストランを出す
	public static ArrayList<RestaurantBeans> getRestaurantBeansListByrestaurantId1(int userId1)
			throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			//レストランの情報を取得したいんじゃなくてuserのが必要から?の所はuser_id
			st = con.prepareStatement(
					"SELECT m_restaurant.restaurant_id,"
							+ " m_restaurant.name,"
							+ " m_restaurant.word,"
							+ " m_restaurant.file_name1,"
							+ " m_restaurant.price,"
							+ " m_restaurant.star"
							+ " from m_restaurant"
							+ " join m_favorite"
							+ " on m_restaurant.restaurant_id=m_favorite.restaurant_id"
							+ " where m_favorite.user_id=?");
			st.setInt(1, userId1);

			ResultSet rs = st.executeQuery();
			ArrayList<RestaurantBeans> RestaurantList = new ArrayList<RestaurantBeans>();

			while (rs.next()) {
				RestaurantBeans restaurant = new RestaurantBeans();
				restaurant.setRestaurantId(rs.getInt("restaurant_id"));
				restaurant.setName(rs.getString("name"));
				restaurant.setWord(rs.getString("word"));
				restaurant.setFileName1(rs.getString("file_name1"));
				restaurant.setPrice(rs.getString("price"));
				restaurant.setStar(rs.getString("star"));

				RestaurantList.add(restaurant);
			}

			System.out.println("searching RestaurantList by RestaurantID has been completed");
			return RestaurantList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}