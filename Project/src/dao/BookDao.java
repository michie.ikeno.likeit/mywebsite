package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BookBeans;

public class BookDao {

	//カラムに登録
	public static void insertBookinfo(BookBeans book) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO m_book(book_id,date,time,people,note,restaurant_id,user_id,) VALUES(?,?,?,?,?,?,?)");
			st.setInt(1, book.getBookId());
			st.setString(2, book.getDate());
			st.setString(3, book.getTime());
			st.setString(4, book.getPeople());
			st.setString(5, book.getNote());
			st.setInt(6, book.getRestaurantId());
			st.setInt(7, book.getUserId());
			st.executeUpdate();
			System.out.println("inserting book has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static ArrayList<BookBeans> getBookBeansListByBookId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_book"
							+ " JOIN m_user"
							+ " ON m_book.user_id = m_user.user_id"
							+ " WHERE m_book.user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			ArrayList<BookBeans> bookDetail = new ArrayList<BookBeans>();

			BookBeans book = new BookBeans();
			while (rs.next()) {
				book.setBookId(rs.getInt("book_id"));
				book.setDate(rs.getString("date"));
				book.setTime(rs.getString("time"));
				book.setPeople(rs.getString("people"));
				book.setNote(rs.getString("note"));

				book.setName(rs.getString("name"));
				book.setName2(rs.getString("name2"));
				book.setMail(rs.getString("mail"));
				book.setTel(rs.getString("tel"));
				bookDetail.add(book);
			}
			System.out.println("searching BookBeans by buyID has been completed");

			return bookDetail;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static ArrayList<BookBeans> getBookBeansListByUserId(int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_book"
							+ " JOIN m_user"
							+ " ON m_book.user_id = m_restaurant.user_id"
							+ " WHERE m_book.user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();

			ArrayList<BookBeans> bookDetail = new ArrayList<BookBeans>();

			BookBeans book = new BookBeans();
			while (rs.next()) {
				book.setBookId(rs.getInt("book_id"));
				book.setDate(rs.getString("date"));
				book.setTime(rs.getString("time"));
				book.setPeople(rs.getString("people"));

				book.setWord(rs.getString("word"));
				book.setFileName1(rs.getString("fileName1"));
				bookDetail.add(book);
			}
			System.out.println("searching BookBeans by buyID has been completed");

			return bookDetail;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void insertBook(BookBeans book, int RestaurantId1, int UserId1) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO m_book(date,time,people,restaurant_id,user_id,note) VALUES(?,?,?,?,?,?)");
			st.setString(1, book.getDate());
			st.setString(2, book.getTime());
			st.setString(3, book.getPeople());
			st.setInt(4, RestaurantId1);
			st.setInt(5, UserId1);
			st.setString(6, book.getNote());

			st.executeUpdate();
			System.out.println("inserting book has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	public static ArrayList<BookBeans> getBookDataBeansByBookId(int Id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_book"
							+ " JOIN m_restaurant"
							+ " ON m_book.restaurant_id=m_restaurant.restaurant_id"
							+ " WHERE m_book.user_id=?");

			st.setInt(1, Id);

			ResultSet rs = st.executeQuery();
			ArrayList<BookBeans> BookList = new ArrayList<BookBeans>();

			while (rs.next()) {
				BookBeans book = new BookBeans();
				book.setUserId(rs.getInt("user_id"));
				book.setBookId(rs.getInt("book_id"));
				book.setDate(rs.getString("date"));
				book.setTime(rs.getString("time"));
				book.setPeople(rs.getString("people"));
				book.setNote(rs.getString("note"));
				book.setRestaurantId(rs.getInt("restaurant_id"));
				book.setName(rs.getString("name"));
				book.setWord(rs.getString("word"));
				book.setFileName1(rs.getString("file_name1"));

				BookList.add(book);
			}

			System.out.println("searching BookBeans by bookID has been completed");

			return BookList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

}
