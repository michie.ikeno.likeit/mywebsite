package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;
@WebServlet("/LoginResultServlet")
public class LoginResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {
			//login.jspのnameから入力した物をパラメーターで取得
			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");
			 
			UserDao userDao1=new UserDao();
			UserBeans userbeans = userDao1.getUserId(loginId,password);

			//userbeansが取得できたらエラー処理
			if (userbeans != null) {
				session.setAttribute("isLogin", true);
				session.setAttribute("userbeans", userbeans);
				//ログイン前のページを取得
				String returnStrUrl = (String) WebHelper.cutSessionAttribute(session, "returnStrUrl");
//ログイン前ページにリダイレクト。指定がない場合Indexのdogetへ
				response.sendRedirect(returnStrUrl!=null?returnStrUrl:"IndexServlet");
			} else {
				session.setAttribute("loginId", loginId);
				session.setAttribute("loginErrorMessage", "入力内容が正しくありません");
				response.sendRedirect("LoginServlet");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}
	}
		}


