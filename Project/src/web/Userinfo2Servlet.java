package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;

/**
 * Servlet implementation class Userinfo2Servlet
 */
@WebServlet("/Userinfo2Servlet")
public class Userinfo2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//ユーザーIDを習得
			//ここも変わらずsessionからの情報を持ってくる
			UserBeans userbeans=(UserBeans)session.getAttribute("userbeans");

			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
						String validationMessage = (String) WebHelper.cutSessionAttribute(session, "validationMessage");


						request.setAttribute("validationMessage", validationMessage);
						request.setAttribute("userbeans",userbeans);

						request.getRequestDispatcher(WebHelper.USERINFO2_PAGE).forward(request, response);



} catch (Exception e) {
	e.printStackTrace();
	session.setAttribute("errorMessage", e.toString());
	response.sendRedirect("ErrorServlet");
}
	}

}
