package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookBeans;

/**
 * Servlet implementation class Book3Servlet
 */
@WebServlet("/Book3Servlet")
public class Book3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {



			String inputNote = request.getParameter("note");
//オブジェクト型(BookBeans)//bookのバルーンの中にnoteと言うものが加わった
			BookBeans book = (BookBeans)session.getAttribute("book");
			book.setNote(inputNote);

			session.setAttribute("book", book);
			request.getRequestDispatcher(WebHelper.BOOK3_PAGE).forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}

	}

}
