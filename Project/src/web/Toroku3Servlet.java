package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class Toroku3Servlet
 */
@WebServlet("/Toroku3Servlet")
public class Toroku3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String inputName = request.getParameter("name");
			String inputName2 = request.getParameter("name2");
			String inputLoginId = request.getParameter("login_id");
			String inputMail = request.getParameter("mail");
			String inputTel = request.getParameter("tel");
			String inputBirthday = request.getParameter("birthday");
			String inputPassword = request.getParameter("password");

			UserBeans udb = new UserBeans();
			udb.setLoginId(inputLoginId);
			udb.setName(inputName);
			udb.setName2(inputName2);
			udb.setMail(inputMail);
			udb.setTel(inputTel);
			udb.setBirthday(inputBirthday);
			udb.setPassword(inputPassword);

			//INSERT文のDaoを呼び出すことで



			UserDao userDao = new UserDao();
			userDao.insertUser(udb);


			request.getRequestDispatcher(WebHelper.TOROKU3_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}
	}
}
