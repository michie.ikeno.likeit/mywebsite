package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class Userinfo3Servlet
 */
@WebServlet("/Userinfo3Servlet")
public class Userinfo3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		//これは大きなsesssionで
		HttpSession session = request.getSession();
try {

	//エラーメッセージを格納する変数
	String validationMessage = "";

//ここでは変更させた情報をgetする
	String inputLoginId =request.getParameter("loginId");
	String inputName=request.getParameter("name");
	String inputName2=request.getParameter("name2");
	String inputMail=request.getParameter("mail");
	String inputTel=request.getParameter("tel");
	String inputBirthday=request.getParameter("birthday");
	String inputPassword=request.getParameter("password");


	//新しく最初にsessionから持ってきたuserbeans新しい情報に変更させたuserbeansにsetする。
	//ここの動き的には("userbeans")は最初の情報が入っていて左のuserbeansが新しい情報が入ったものになる。これで前の情報を上書きする形になった
	//もし全く新しいものにしたい場合はUserBeans a = new Userbeans();にすればいい
	UserBeans userbeans=(UserBeans) session.getAttribute("userbeans");

	userbeans.setLoginId(inputLoginId);
	userbeans.setName(inputName);
	userbeans.setName2(inputName2);
	userbeans.setMail(inputMail);
	userbeans.setTel(inputTel);
	userbeans.setBirthday(inputBirthday);
	userbeans.setPassword(inputPassword);

	//ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
		if (!WebHelper.isLoginIdValidation(userbeans.getLoginId())) {
			validationMessage = "半角英数とハイフン、アンダースコアのみ入力できます";
		}
		//loginIdの重複をチェック
		if ( UserDao.isOverlapLoginId(userbeans.getLoginId(),userbeans.getUserId())) {
			validationMessage = "ほかのユーザーが使用中のログインIDです";
		}
		//バリデーションエラーメッセージがないなら確認画面へ
		if(validationMessage.length() == 0){
			//確認画面へ
			request.setAttribute("userbeans",userbeans);
			request.getRequestDispatcher(WebHelper.USERINFO4_PAGE).forward(request, response);
		}else {
			//セッションにエラーメッセージを持たせてユーザー画面へ
		request.setAttribute("validationMessage", validationMessage);
			request.setAttribute("userbeans",userbeans);
			request.getRequestDispatcher(WebHelper.USERINFO2_PAGE).forward(request, response);

		}
			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("ErrorServlet");
			}
			}

		}





