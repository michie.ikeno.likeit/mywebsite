package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantBeans;
import dao.RestaurantDao;

/**
 * Servlet implementation class IndexServlet
 */


@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//商品情報を取得//ランダムに９個出てくるよ
			ArrayList<RestaurantBeans> restaurantList = RestaurantDao.getRandItem(9);
			//リクエストスコープにセット
			request.setAttribute("restaurantList", restaurantList);

			//セッションにsearchRestaurantが入っていたら破棄する
//検索のsearchRestaurantとsearchPriceをindex.jspへ持っていく
			String searchRestaurant = (String)session.getAttribute("searchRestaurant");
			if(searchRestaurant != null) {
				session.removeAttribute("searchRestaurant");
			}
			String searchPrice = (String)session.getAttribute("searchPrice");
			if(searchPrice != null) {
				session.removeAttribute("searchPrice");
			}
			request.getRequestDispatcher(WebHelper.INDEX_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}
	}



protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
	request.setCharacterEncoding("UTF-8");
	HttpSession session = request.getSession();

	try {
		//登録後ここに戻る(登録完了ページの情報を習得)
		
		request.getParameter("Userinfo4Servlet");

		request.getRequestDispatcher(WebHelper.INDEX_PAGE).forward(request, response);

	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("ErrorServlet");
	}

}

}


