package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantBeans;
import dao.RestaurantDao;

@WebServlet("/Restaurantinfo")
public class Restaurantinfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			//選択された商品のIDを型変換し利用//getParameterはString型しか取れないだからintにしたい場合は2行目の所を必ず書くこと
			String restaurant_id = request.getParameter("restaurant_id");
			int restaurant_id1 = Integer.parseInt(restaurant_id);

			//対象のアイテム情報を取得//int型になったものをここに入れる
			RestaurantBeans restaurant = RestaurantDao.getRestaurantByRestaurantID(restaurant_id1);

			//リクエストパラメーターにセット//jspへ
			request.setAttribute("restaurant", restaurant);

			//履歴を取得
			ArrayList<RestaurantBeans> history = (ArrayList<RestaurantBeans>) session.getAttribute("history");

			//セッションに閲覧履歴情報がない場合閲覧履歴情報を作成
			if (history == null) {
				history = new ArrayList<RestaurantBeans>();
			}
			//閲覧履歴にレストレランを追加。
			history.add(restaurant);
			//レストランの閲覧履歴の情報更新
			session.setAttribute("history", history);

			request.getRequestDispatcher(WebHelper.RESTAURANT_PAGE).forward(request, response);
			//ここのtrycatchはもしエラーが出ちゃった時の保険
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}
	}

}