package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

@WebServlet("/Toroku2Servlet")
public class Toroku2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			String inputName = request.getParameter("name");
			String inputName2 = request.getParameter("name2");
			String inputLoginId = request.getParameter("login_id");
			String inputMail = request.getParameter("mail");
			String inputTel = request.getParameter("tel");
			String inputBirthday = request.getParameter("birthday");
			String inputPassword = request.getParameter("password");
			String inputConfirmPassword = request.getParameter("confirmpassword");

			UserBeans udb = new UserBeans();

			udb.setName(inputName);
			udb.setName2(inputName2);
			udb.setLoginId(inputLoginId);
			udb.setMail(inputMail);
			udb.setTel(inputTel);
			udb.setBirthday(inputBirthday);
			udb.setPassword(inputPassword);

			String validationMessage = "";
			// 入力されているパスワードが確認用と等しいかの確認
			if (!inputPassword.equals(inputConfirmPassword)) {
				validationMessage += "入力されているパスワードと確認用パスワードが違います";
			}

			// ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
			if (!WebHelper.isLoginIdValidation(udb.getLoginId())) {
				validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます";
			}
			// loginIdの重複をチェック
			if (UserDao.isOverlapLoginId(udb.getLoginId(), 0)) {
				validationMessage += "ほかのユーザーが使用中のログインIDです";
			}

			// バリデーションエラーメッセージがないなら確認画面へ
			if (validationMessage.length() == 0) {
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(WebHelper.TOROKU2_PAGE).forward(request, response);
				return;
			} else {
				request.setAttribute("udb", udb);
				request.setAttribute("validationMessage", validationMessage);
				request.getRequestDispatcher(WebHelper.TOROKU1_PAGE).forward(request, response);
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");

		}
	}
}
