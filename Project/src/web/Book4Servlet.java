package web;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookBeans;
import beans.UserBeans;
import dao.BookDao;

/**
 * Servlet implementation class Book4Servlet
 */
@WebServlet("/Book4Servlet")
public class Book4Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Book4Servlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//オブジェクト型だったのを元の型のStringに戻す
		try {

			BookBeans book = (BookBeans) session.getAttribute("book");

			String restaurantId = (String) session.getAttribute("restaurantId");
			int restaurantId1 = Integer.parseInt(restaurantId);

			//userbeansに変更
			UserBeans userbeans = (UserBeans) session.getAttribute("userbeans");
			int userId1 = userbeans.getUserId();

			BookDao bookDao = new BookDao();
			bookDao.insertBook(book, restaurantId1, userId1);

			request.getRequestDispatcher(WebHelper.BOOK4_PAGE).forward(request, response);

		} catch (SQLException e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");

		}

	}
}
