package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class Userinfo4Servlet
 */
@WebServlet("/Userinfo4Servlet")
public class Userinfo4Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		//これは大きなsesssionで
		HttpSession session = request.getSession();

		try {
			UserBeans userbeans=(UserBeans) session.getAttribute("userbeans");
			//アップデート
			UserDao userDao = new UserDao();
			UserDao.updateUser(userbeans);


			request.getRequestDispatcher(WebHelper.USERINFO3_PAGE).forward(request, response);

		} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("ErrorServlet");
	}
	}
}
