package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantBeans;
import beans.UserBeans;
import dao.RestaurantDao;

/**
 * Servlet implementation class FavoriteServlet
 */
@WebServlet("/FavoriteServlet")
public class FavoriteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//session
			UserBeans userbeans = (UserBeans) session.getAttribute("userbeans");
			int userId1 = userbeans.getUserId();

			//詳細のボタンクリック時→restaurantIdを持ってくる
			//indexのボタンクリック時→restaurantIdは無い

			//restaurantIdが無い場合はif文は通らずに次の文に飛んで出力だけさせる

			//詳細ページのボタンからお気に入りのお店の情報を登録
			String restaurantId = request.getParameter("restaurantId");
			//resutaurantIdがnullじゃない場合は詳細ページから遷移されているので、お気に入り登録をする
			if (restaurantId != null) {
				int restaurantId1 = Integer.parseInt(restaurantId);
				RestaurantDao.insertfavoriteRestaurant(restaurantId1, userId1);
			}

			//お気に入りに登録されているお店の情報を取得
			RestaurantDao a = new RestaurantDao();
			ArrayList<RestaurantBeans> restaurant = a.getRestaurantBeansListByrestaurantId1(userId1);

			request.setAttribute("restaurant", restaurant);

			request.getRequestDispatcher(WebHelper.FAVORITE_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}

	}

}
