package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantBeans;
import dao.RestaurantDao;

/**
 * Servlet implementation class HistoryServlet
 */
@WebServlet("/Searchresult")
public class Searchresult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する商品数


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			String searchRestaurant = request.getParameter("search_restaurant");
			String searchPrice = request.getParameter("search_price");
			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("searchRestaurant", searchRestaurant);
			session.setAttribute("searchPrice",searchPrice);

			// 商品リストを取得 ページ表示分のみ
						ArrayList<RestaurantBeans> searchResultRestaurantList = RestaurantDao.getRestaurantByPlaceName(searchRestaurant,searchPrice);

						// 検索ワードに対しての総ページ数を取得
						double restaurantCount = RestaurantDao.getRestaurantCount(searchRestaurant,searchPrice);
						int pageMax = (int) Math.ceil(restaurantCount );


						//総アイテム数
						request.setAttribute("restaurantCount", (int) restaurantCount);
						request.setAttribute("RestaurantList", searchResultRestaurantList);

						// 総ページ数
						request.setAttribute("pageMax", pageMax);
						// 表示ページ
						request.setAttribute("pageNum", pageNum);
						request.setAttribute("RestaurantList", searchResultRestaurantList);

						request.getRequestDispatcher(WebHelper.SEARCH_RESULT_PAGE).forward(request, response);

		} catch (Exception e) {
						e.printStackTrace();
						session.setAttribute("errorMessage", e.toString());
						response.sendRedirect("ErrorServlet");
					}
				}



		}