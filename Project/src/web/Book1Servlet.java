package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Book1Servlet
 */
@WebServlet("/Book1Servlet")
public class Book1Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		// セッション開始
		HttpSession session = request.getSession();
try {
		//レストランIDをrestaurantinfoから取得
					String restaurantId = request.getParameter("restaurantId");
					System.out.println("restaurantId:"+restaurantId);

					session.setAttribute("restaurantId", restaurantId);






		//book1のjspへ
		request.getRequestDispatcher(WebHelper.BOOK1_PAGE).forward(request, response);



}

		 catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("ErrorServlet");
	}
}

}