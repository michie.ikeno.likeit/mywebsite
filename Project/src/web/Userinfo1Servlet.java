package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;

/**
 * Servlet implementation class Userinfo1Servlet
 */
@WebServlet("/Userinfo1Servlet")
public class Userinfo1Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//ユーザーIDを習得
			//sessionでユーザー情報を持ってくる
			UserBeans userbeans=(UserBeans)session.getAttribute("userbeans");

			request.setAttribute("userbeans",userbeans);


			request.getRequestDispatcher(WebHelper.USERINFO1_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}

	}
}
