package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.RestaurantBeans;

/**
 * Servlet implementation class FavoriteServlet
 */
@WebServlet("/HistoryServlet")
public class HistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//お店の情報を習得
			ArrayList<RestaurantBeans> history = (ArrayList<RestaurantBeans>) session.getAttribute("history");

			//閲覧履歴にお店が入っていないなら
			if (history == null) {
				String restaurantActionMessage = "閲覧したレストランはございません";
				request.setAttribute("restaurantActionMessage", restaurantActionMessage);
			}

			request.getRequestDispatcher(WebHelper.HISTORY_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}

	}
}
