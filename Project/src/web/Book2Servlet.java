package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookBeans;

/**
 * Servlet implementation class Book2Servlet
 */
@WebServlet("/Book2Servlet")
public class Book2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		//これは大きなsesssionで
		HttpSession session = request.getSession();



		try {

		String inputDate = request.getParameter("date");
		String inputTime = request.getParameter("time");
		String inputPeople = request.getParameter("people");

//大きなsessionの中からbookっていう小さいバルーンの中にgetparameterで取ってきたものを入れる。
		BookBeans book = new BookBeans();


		book.setDate(inputDate);
		book.setTime(inputTime);
		book.setPeople(inputPeople);
//ここでbookというバルーンの中に入れることができた。


		session.setAttribute("book", book);

		request.getRequestDispatcher(WebHelper.BOOK2_PAGE).forward(request, response);



	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("ErrorServlet");
	}
}


}
