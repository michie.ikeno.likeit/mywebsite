package web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BookBeans;
import beans.UserBeans;
import dao.BookDao;

/**
 * Servlet implementation class BookedServlet
 */
@WebServlet("/BookedServlet")
public class BookedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//予約の情報を習得

			UserBeans userbeans = (UserBeans) session.getAttribute("userbeans");

			//必要な情報//userIdはint型だからgetBookDataBeansByBookIdもint型にする
			BookDao a = new BookDao();
			ArrayList<BookBeans> book = a.getBookDataBeansByBookId(userbeans.getUserId());

			request.setAttribute("book", book);

			request.getRequestDispatcher(WebHelper.BOOKED_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}
	}

	//ここはindexのボタンが飛んできたのを受け取る
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			UserBeans userbeans = (UserBeans) session.getAttribute("userbeans");

			//必要な情報//userIdはint型だからgetBookDataBeansByBookIdもint型にする
			BookDao a = new BookDao();
			ArrayList<BookBeans> book = a.getBookDataBeansByBookId(userbeans.getUserId());

			request.setAttribute("book", book);

			request.getRequestDispatcher(WebHelper.BOOKED_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("ErrorServlet");
		}
	}
}
