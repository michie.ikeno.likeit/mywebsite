package web;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpSession;

/**
 * 定数保持、処理及び表示簡略化ヘルパークラス
 *
 * @author d-yamaguchi
 *
 */
public class WebHelper {
	// エラーページ//
		static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	// TOPページ
	static final String INDEX_PAGE = "/WEB-INF/jsp/index.jsp";
	// 予約(日付など）
	static final String BOOK1_PAGE = "/WEB-INF/jsp/book1.jsp";
	//予約個人情報
	static final String BOOK2_PAGE = "/WEB-INF/jsp/book2.jsp";
	//予約(確認)
	static final String BOOK3_PAGE = "/WEB-INF/jsp/book3.jsp";
	//予約（承ったよ）
		static final String BOOK4_PAGE = "/WEB-INF/jsp/book4.jsp";
	//ユーザー予約済みページ
	static final String BOOKED_PAGE = "/WEB-INF/jsp/booked.jsp";
	//ユーザーお気に入りページ
	static final String FAVORITE_PAGE = "/WEB-INF/jsp/favorite.jsp";
	//ユーザー閲覧履歴ページ
	static final String HISTORY_PAGE = "/WEB-INF/jsp/history.jsp";
	// ログイン
	static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
	//ログアウト
	static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
	// 店舗情報ページ
	static final String ITEM_PAGE = "/WEB-INF/jsp/restaurantinfo.jsp";
	// 検索結果
	static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/searchresult.jsp";
	// 新規登録
	static final String TOROKU1_PAGE = "/WEB-INF/jsp/toroku1.jsp";
	// 新規登録入力内容確認
	static final String TOROKU2_PAGE = "/WEB-INF/jsp/toroku2.jsp";
	// 新規登録完了
	static final String TOROKU3_PAGE = "/WEB-INF/jsp/toroku3.jsp";
	// ユーザー情報
		static final String USERINFO1_PAGE = "/WEB-INF/jsp/userinfo1.jsp";
		// ユーザー情報更新確認
		static final String USERINFO2_PAGE = "/WEB-INF/jsp/userinfo2.jsp";
		// ユーザー情報更新完了こっち後
		static final String USERINFO3_PAGE = "/WEB-INF/jsp/userinfo3.jsp";
		//本当はこっちが先
		static final String USERINFO4_PAGE = "/WEB-INF/jsp/userinfo4.jsp";
		//レストラン情報
		static final String RESTAURANT_PAGE="/WEB-INF/jsp/restaurantinfo.jsp";

	public static WebHelper getInstance() {
		return new WebHelper();
	}




	/**
	 * ハッシュ関数
	 *
	 * @param target
	 * @return
	 */
	public static String getSha256(String target) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(target.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	 * ログインIDのバリデーション
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		return false;

	}


}
