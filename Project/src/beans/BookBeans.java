package beans;

import java.io.Serializable;

public class BookBeans  implements Serializable {
	private int bookId;
	private String date;
	private String time;
	private String people;
	private int restaurantId;
	private int restaurantId1;
	private int userId;
	private String note;

	private String name;
	private String name2;
	private String mail;
	private String tel;

	private String word;
	private String fileName1;






	public int getBookId() {
		return bookId;

	}
	public void setBookId(int bookId) {
		this.bookId = bookId;

}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date=date;
	}


	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time=time;
	}


	public String getPeople() {
		return people;
	}
	public void setPeople(String people) {
		this.people=people;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note=note;
	}


	public int getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(int restaurantId) {
		this.restaurantId=restaurantId;
	}

	public int getRestaurantId1() {
		return restaurantId1;
	}
	public void setRestaurantId1(int restaurantId1) {
		this.restaurantId1=restaurantId1;
	}


	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
	this.userId=userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
	this.name=name;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
	this.name2=name2;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
	this.mail=mail;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
	this.tel=tel;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public String getFileName1() {
		return fileName1;
	}
	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}
}

