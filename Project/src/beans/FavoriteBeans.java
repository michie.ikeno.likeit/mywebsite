package beans;

import java.io.Serializable;

public class FavoriteBeans implements Serializable {
	private int favoriteId;
	private int restaurantId;
	private int userId;

	public int getFavoriteId() {
		return favoriteId;
	}
	public void setFavoriteId(int favoriteId) {
		this.favoriteId = favoriteId;
	}
	public int getrestaurantId() {
		return restaurantId;
	}
	public void setrestaurantId(int restaurantId) {
		this.restaurantId = restaurantId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
}
