package beans;

import java.io.Serializable;


public class UserBeans implements Serializable {
	private int userId;
	private String name;
	private String name2;
	private String loginId;
	private String mail;
	private String tel;
	private String birthday;
	private String password;
	private String confirmpassword;

	// 全てのデータをセットするコンストラクタ
	public UserBeans(int userId, String name, String name2, String loginId,String mail,String tel,String birthday,String password,String confirmpassword) {
			// TODO 自動生成されたコンストラクター・スタブ
		this.userId = userId;
		this.name = name;
		this.name2 = name2;
		this.loginId = loginId;
		this.mail=mail;
		this.tel = tel;
		this.birthday=birthday;
		this.password = password;
		this.confirmpassword=confirmpassword;

		}
	public UserBeans() {
		this.name = "";
		this.name2 = "";
		this.loginId = "";
		this.mail="";
		this.tel="";
		this.birthday="";
		this.password = "";
		this.confirmpassword="";

	}
	public UserBeans(int userId, String name, String name2, String loginId,String mail,String tel,String birthday,String password) {
	this.userId = userId;
	this.name = name;
	this.name2 = name2;
	this.loginId = loginId;
	this.mail=mail;
	this.tel = tel;
	this.birthday=birthday;
	this.password = password;

	}


	public int getUserId() {
		return userId;

	}
	public void setUserId(int userId) {
		this.userId = userId;
}
	public String getName() {
		return name;

	}
	public void setName(String name) {
		this.name = name;

}
	public String getName2() {
		return name2;

	}
	public void setName2(String name2) {
		this.name2 = name2;
}
	public String getLoginId() {
		return loginId;

	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
}
	public String getMail() {
		return mail;

	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTel() {
		return tel;

	}
	public void setTel(String tel) {
		this.tel = tel;
}
	public String getBirthday() {
		return birthday;

	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPassword() {
		return password;

	}
	public void setPassword(String password) {
		this.password = password;
}

public String getConfirmPassword() {
	return confirmpassword;

}
public void setConfirmPassword(String confirmpassword) {
	this.confirmpassword = confirmpassword;
}





/**
 * ユーザー情報更新時の必要情報をまとめてセットするための処理
 *
 * @param loginId
 * @param name
 * @param name2
 * @param mail
 * @param tel
 * @param birthday
 * @param password
 * @param userId
 *
 */
public void UserBeans(String loginId, String name,String name2, String mail,String tel,String birthday,String password,int userId) {

	this.loginId = loginId;
	this.name = name;
	this.name2=name2;
	this.mail=mail;
	this.tel=tel;
	this.birthday=birthday;
	this.password=password;
	this.userId = userId;
}

}

